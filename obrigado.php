<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="index.css" rel="stylesheet" type="text/css">
    <title>PickNotes.co - Automação de gestão para reembolso institucional</title>
  </head><body>
    <div class="cover">
      <div class="cover-image" style="background-image : url('img/bg-progressive.jpg')"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <img src="img/logo-white2.png" class="center-block img-responsive">
            <h1 class="text-inverse cover-title">Nota Enviada</h1>
            <?php
            (isset($_GET['m']) && !empty($_GET['m'])) ?
              $nome = trim(base64_decode($_GET['m'])) :
              $nome = '';

            $subject = urlencode('Nota '.$nome);
            ?>

            <?php
            if (!empty($nome)) { ?>
              <div style="background-color: white;padding: 10px;margin-bottom: 10px;">
                <img src="app/uploads/<?php echo $nome ?>" alt="<?php echo $nome ?>" style="max-width: 200px;">
              </div>
            <?php } ?>

            <a class="btn btn-warning btn-lg" href="mailto:lucas@lucasmoreira.com.br&subject=<?php echo $subject ?>">Clique aqui para enviar a solicitação de análise</a>
          </div>
        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
              <h1 class="text-center main-title">Planos na Medida Certa</h1>
              <!-- <h2 class="text-center">Nossos planos foram pensados para solucionar as necessidades da sua empresa com inteligência.</h2> -->
              <img src="img/pricing.jpg" class="center-block img-responsive">
          </div>
          <div class="col-md-12 text-center">
              <a href="app/" class="btn btn-warning btn-lg" style="padding: 20px 26px">Comece Grátis</a>
          </div>
        </div>
      </div>
    </div>
    <div class="section bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center">Cadastre-se para Conhecer</h1>
            <div class="col-md-6 text-center col-lg-offset-3">
              <!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<div id="mc_embed_signup">
<form action="https://certificawp.us9.list-manage.com/subscribe/post?u=8a2b906c22707dde5c2c005aa&amp;id=b4cbc81d46" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	
<div class="mc-field-group">
	<input type="email" value="" name="EMAIL" class="required email form-control input-lg" placeholder="Seu e-mail aqui" id="mce-EMAIL">
</div>
<br>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8a2b906c22707dde5c2c005aa_b4cbc81d46" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Cadastrar para Novidades" name="subscribe" id="mc-embedded-subscribe" class="btn btn-default"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
            </div>
          </div>
        </div>
      </div>
    </div>
  

</body></html>